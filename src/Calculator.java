package src;

public class Calculator
{
	public int add(int a,int b)
	{
		int add = a+b;
		return add;
	}
	
	public int substract(int a,int b)
	{
		int substract = a-b;
		return substract;
	}
	
	public int multiply(int a,int b)
	{
		int multiply = a*b;
		return multiply;
	} 
	
	public int divide(int a,int b)
	{
		int divide = a/b;
		return divide;
	}
	static int m,n,p,q;
	float x,y;
	
	public static void main(String[] args)
	{
		Calculator calculator = new Calculator();
		m = calculator.add(10,20);
		n = calculator.substract(30,10);
		p = calculator.multiply(2,3);
		q = calculator.divide(20,2);
		System.out.println(" Addition:"+m+"\n Substraction:"+n+"\n Multiplication:"+p+"\n Division:"+q);
	}

}